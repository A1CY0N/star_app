	// create the module and name it scotchApp
	var scotchApp = angular.module('scotchApp', ['ngRoute']);

	// configure our routes
	scotchApp.config(function ($routeProvider) {
		$routeProvider

			// route for the home page
			.when('/', {
				templateUrl: 'pages/home.html',
				controller: 'mainController'
			})

			// route for the schedule page
			.when('/schedule', {
				templateUrl: 'pages/schedule.html',
				controller: 'schedule'
			})

			// route for the busline page
			.when('/busline', {
				templateUrl: 'pages/busline.html',
				controller: 'busLines'
			});
	});

	// create the controller and inject Angular's $scope
	scotchApp.controller('mainController', function ($scope) {
		// create a message to display in our view
		$scope.message = 'Bienvenue sur l\'application où est mon bus';
	});

	scotchApp.controller('schedule', function ($scope, $http, $sce) {
		$scope.selected_bus = 0;
		$http.get("https://data.explore.star.fr/api/records/1.0/search/?dataset=tco-bus-topologie-lignes-td&facet=nomfamillecommerciale&rows=50").then(function (response) {
			$scope.liste = response.data.records;
		});
		$scope.selectBus = function () {
			$scope.bus_view = $scope.selected_bus;
			$scope.scheduleURL = $sce.trustAsResourceUrl("https://data.explore.star.fr/explore/embed/dataset/tco-bus-circulation-passages-tr/map/?sort=nomcourtligne&refine.nomcourtligne=" + $scope.selected_bus + "&location=14,48.12203,-1.66698&basemap=jawg.streets&static=false&datasetcard=false");
			console.log($scope.scheduleURL);
		}

	});

	scotchApp.controller('busLines', function ($scope, $http, $sce) {
		var map = L.map('mapid').setView([48.08, -1.68], 12);
		var test = [];

		L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
			id: 'mapbox.streets'
		}).addTo(map);
		$scope.selected_bus = 0;
		$http.get("https://data.explore.star.fr/api/records/1.0/search/?dataset=tco-bus-topologie-lignes-td&facet=nomfamillecommerciale&rows=200").then(function (response) {
			$scope.liste = response.data.records;
		});
		$scope.selectBus = function () {
			$scope.bus_view = $scope.selected_bus;
			$http.get("https://data.explore.star.fr//api/records/1.0/search/?dataset=tco-bus-topologie-parcours-td&facet=nomcourtligne&facet=senscommercial&facet=type&facet=nomarretdepart&facet=nomarretarrivee&facet=estaccessiblepmr&refine.nomcourtligne=" + $scope.selected_bus).success(function (data, status) {
				$scope.test = data.records[0].fields.parcours;
				console.log("données récup", $scope.test);
				test = $scope.test;
				var courbe = {
					"type": "FeatureCollection",
					"features": [{
						"type": "Feature",
						"geometry": test,
						"properties": {
							"line": $scope.selected_bus,
							"reseau": "STAR",
							"SHAPE_Leng": "53224.7785428",
							"Completion": "100"
						}
					}]
				};
				console.log("test1 var données recup", test);
				console.log("test3 var ma propre geo", courbe);
	
				L.geoJson(courbe, {
					style: function (feature) {
						return {
							stroke: true,
							color: "blue",
							weight: 5
						};
					},
	
					onEachFeature: function (feature, layer) {
						layer.bindPopup("Ligne : " + feature.properties.line + "<br>" +
							"Réseau : " + feature.properties.reseau);
					}
				}).addTo(map);
			});



		}


	});