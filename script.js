	// create the module and name it scotchApp
	var scotchApp = angular.module('scotchApp', ['ngRoute']);

	// configure our routes
	scotchApp.config(function ($routeProvider) {
		$routeProvider

			// route for the home page
			.when('/', {
				templateUrl: 'pages/home.html',
				controller: 'mainController'
			})

			// route for the schedule page
			.when('/schedule', {
				templateUrl: 'pages/schedule.html',
				controller: 'schedule'
			})

			// route for the busline page
			.when('/busline', {
				templateUrl: 'pages/busline.html',
				controller: 'busLines'
			});
	});

	// create the controller and inject Angular's $scope
	scotchApp.controller('mainController', function ($scope) {
		// create a message to display in our view
		$scope.message = 'Bienvenue sur l\'application où est mon bus';
	});

	scotchApp.controller('schedule', function ($scope, $http, $sce) {
		$scope.selected_bus = 0;
		$http.get("https://data.explore.star.fr/api/records/1.0/search/?dataset=tco-bus-topologie-lignes-td&facet=nomfamillecommerciale&rows=50").then(function (response) {
			$scope.liste = response.data.records;
		});
		$scope.selectBus = function () {
			$scope.bus_view = $scope.selected_bus;
			$scope.scheduleURL = $sce.trustAsResourceUrl("https://data.explore.star.fr/explore/embed/dataset/tco-bus-circulation-passages-tr/map/?sort=nomcourtligne&refine.nomcourtligne=" + $scope.selected_bus + "&location=14,48.12203,-1.66698&basemap=jawg.streets&static=false&datasetcard=false");
			console.log($scope.scheduleURL);
		}

	});

	scotchApp.controller('busLines', function ($scope, $http, $sce) {
		var map = L.map('mapid').setView([48.08, -1.68], 12);
		var coord_parcours_line_api = [];
		var courbe;
		var display_line = false;
		var bus_lines = [];
		var bus_position = [];
		var liste_id_arret;
		L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
			id: 'mapbox.streets'
		}).addTo(map);
		$scope.selected_bus = 0;
		$scope.selected_bus_stop = 0;
		$http.get("https://data.explore.star.fr/api/records/1.0/search/?dataset=tco-bus-topologie-lignes-td&facet=nomfamillecommerciale&rows=200").then(function (response) {
			$scope.liste = response.data.records;
		});
		$scope.selectBus = function () {
			$scope.bus_view = $scope.selected_bus;
			display_line = false; /* Variable qui permet d'effacer les tracés à chaque fois qu'on change de ligne */
			$http.get("https://data.explore.star.fr//api/records/1.0/search/?dataset=tco-bus-topologie-parcours-td&facet=nomcourtligne&facet=senscommercial&facet=type&facet=nomarretdepart&facet=nomarretarrivee&facet=estaccessiblepmr&refine.nomcourtligne=" + $scope.selected_bus).success(function (data, status) {
				/* Coordonnées de la ligne de bus */
				coord_parcours_line_api = data.records[0].fields.parcours;
				display_line = true;
				courbe = {
					"type": "FeatureCollection",
					"features": [{
						"type": "Feature",
						"geometry": coord_parcours_line_api,
						"properties": {
							"line": $scope.selected_bus,
							"reseau": "STAR",
							"SHAPE_Leng": "53224.7785428",
							"Completion": "100"
						}
					}]
				};
				bus_lines = L.geoJson(courbe, {
					style: function (feature) {
						return {
							stroke: true,
							color: "red",
							weight: 5
						};
					},

					onEachFeature: function (feature, layer) {
						layer.bindPopup("Ligne : " + feature.properties.line + "<br>" +
							"Réseau : " + feature.properties.reseau);
					}
				}).addTo(map);
			});
			$http.get("https://data.explore.star.fr/api/records/1.0/search/?dataset=tco-bus-topologie-dessertes-td&rows=200&sort=idparcours&facet=libellecourtparcours&facet=nomarret&refine.nomcourtligne=" + $scope.selected_bus).then(function (response) {
				/* Liste des arrêts de bus de la ligne sélectionnée*/
				$scope.liste_arret = response.data.facet_groups[0].facets;
				liste_id_arret = response.data.records;
				/*for (i in $scope.liste_arret) {
					//console.log("name", $scope.liste_arret[i].name);
				}
				for (i in liste_id_arret) {
					console.log("id", liste_id_arret[i].fields.idarret);
				}*/
			});

			$http.get("https://data.explore.star.fr/api/records/1.0/search/?dataset=tco-bus-topologie-pointsarret-td&rows=-1").then(function (response) {
				/* Liste de tous les arrêts de bus */
				liste_all_arret = response.data.records;
				for (j in $scope.liste_arret) {
					//console.log("tata", liste_all_arret[i].fields.nom);
					for (i in liste_all_arret) {
						//console.log($scope.liste_arret[j].name, (liste_id_arret[j].fields.nomarret).sort())
						//if ($scope.liste_arret[j].name == liste_id_arret[j].fields.nomarret) {
						//console.log("totototo", $scope.liste_arret[i].name)
						//console.log("test", liste_id_arret[j].fields.idarret);
						if (liste_all_arret[i].fields.code == liste_id_arret[j].fields.idarret) {
							stop = {
								"type": "FeatureCollection",
								"features": [{
									"type": "Feature",
									"geometry": liste_all_arret[i].geometry,
									"properties": {
										"State": "test",
										"NameStop": liste_all_arret[i].fields.nom,

									}
								}]
							};
							var geojsonMarkerOptions = {
								radius: 8,
								fillColor: "#ff7800",
								color: "#000",
								weight: 1,
								opacity: 1,
								fillOpacity: 1
							};
							var busStopIcon = L.icon({
								iconUrl: 'stop.png',
								iconSize: [32, 37],
								iconAnchor: [16, 37],
								popupAnchor: [0, -28]
							});
							bus_stop = L.geoJson(stop, {
								pointToLayer: function (feature, latlng) {
									return L.circleMarker(latlng, geojsonMarkerOptions);
									/*return L.marker(latlng, {
										icon: busStopIcon
									});*/
								},
								onEachFeature: function (feature, layer) {
									layer.bindPopup("Nom de l'arrêt : " + feature.properties.NameStop + "<br>");
								}
							}).addTo(map);
						}
						//}
					}
				}
			});


			$http.get("https://data.explore.star.fr/api/records/1.0/search/?dataset=tco-bus-vehicules-position-tr&facet=numerobus&facet=etat&facet=nomcourtligne&facet=sens&facet=destination&refine.nomcourtligne=" + $scope.selected_bus).success(function (data, status) {
				/* Coordonnées de la position des bus*/
				if (data.records[0] == null) {
					alert("La ligne " + $scope.selected_bus + " ne circule pas actuellement");
				} else {
					coord_bus_api = data.records[0].geometry;
					bus = {
						"type": "FeatureCollection",
						"features": [{
							"type": "Feature",
							"geometry": coord_bus_api,
							"properties": {
								"State": data.records[0].fields.etat,
								"Bus_number": data.records[0].fields.numerobus,
								"Bus_name": data.records[0].fields.nomcourtligne,
								"Time": data.records[0].fields.ecartsecondes,
								"Destination": data.records[0].fields.destination
							}
						}]
					};
					var geojsonMarkerOptions = {
						radius: 8,
						fillColor: "#ff7800",
						color: "#000",
						weight: 1,
						opacity: 1,
						fillOpacity: 1
					};
					var busIcon = L.icon({
						iconUrl: 'bus.png',
						iconSize: [32, 37],
						iconAnchor: [16, 37],
						popupAnchor: [0, -28]
					});
					bus_position = L.geoJson(bus, {
						pointToLayer: function (feature, latlng) {
							//return L.circleMarker(latlng, geojsonMarkerOptions);
							return L.marker(latlng, {
								icon: busIcon
							});
						},
						onEachFeature: function (feature, layer) {
							layer.bindPopup("Etat : " + feature.properties.State + "<br>" +
								"Numéro du bus : " + feature.properties.Bus_number + "<br>" +
								"Nom du bus : " + feature.properties.Bus_name + "<br>" +
								"Destination : " + feature.properties.Destination + "<br>" +
								"Ecart (secondes) : " + feature.properties.time);
						}
					}).addTo(map);




					/*L.geoJson(bus, {
						style: function (feature) {
							return {
								color: "#000000",
								weight: 0.25,
								opacity: 0.5,
								fillOpacity: 10
								
								
							};
						},

					}).addTo(map);*/
				}
			});
			if (display_line == false) {
				/* Supprime le tracé quand on change de ligne */
				map.removeLayer(bus_lines);
				map.removeLayer(bus_position);
				map.removeLayer(bus_stop);
			}

		}
	});