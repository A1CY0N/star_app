# Star_app

## Description

AngularJS web application that gives timetables and bus lines of the STAR bus network of Rennes city.

## Installation

git clone https://gitlab.com/A1CY0N/star_app.git

## Use

Just use your favorite browser (understand firefox !!) and open index.html